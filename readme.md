# My Game Plan Server - Demo

## Project Setup

1. Create a MongoDB database with 4 empty collections: `matches`, `events`, `players` and `clubs`
2. Create a database user with Admin rights
3. Copy the database's connection string, it should like this:
   `mongodb+srv://root:<password>@mgp-devcluster.6vvqn.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`
4. Clone this repository
5. Run `yarn` to install all packages
6. Copy the `.env.example` file and name it `.env`. Replace the DATABASE_URL property with your connection string. Make sure to replace the username (root) and password too.
7. Run `yarn start` to start the server.
   If your database is empty, you can run `yarn start --seed` which will upload the json files to the right collections.

## Extra Context

AKA stuff Jessy should've probably done but didn't do because of the time

### TypeScript

TypScript = life 😻. But figuring out the right way to use TypeScript with third parties can be a litle time consuming, so I just skipped that for now.

### Error handling + validation ❌

Obviously, every network request in production should handle errors: return the right error code when a request fails. For time saving purposes, I just assumed that for the first time in the history of development every request will succeed. 🙈

### Divide data fetching into multiple calls

Loading all events for all clubs isn't the most scalable approach. For now it's okay but fetching data from the front end is gonna take a while as a lot of data will probably be fetched at once.
