const mongoose = require("mongoose");

const DATABASE_URL = process.env.DATABASE_URL;

async function connectDatabase() {
  return new Promise((resolve, reject) => {
    return mongoose.connect(DATABASE_URL, function (error) {
      if (error) {
        console.log("[config/db.js] Unable to connect to database");
        reject();
      } else {
        console.log("[config/db.js] Successfully connected to database!");
        resolve();
      }
    });
  });
}

const _db = mongoose.connection;
_db.on("error", console.error.bind(console, "Failed to connect MongoDB"));

module.exports = connectDatabase;
