const CLUBS = require("../../dummy-data/clubs.json");
const PLAYERS = require("../../dummy-data/players.json");
const EVENTS = require("../../dummy-data/events.json");
const MATCHES = require("../../dummy-data/match_calendar.json");

async function seed(context) {
  console.log("Starting seed for clubs, players, events and matches");

  try {
    // Matches
    const _matchesPromise = await context.models.Match.insertMany(MATCHES);
    console.log("Added data for", MATCHES.length, "matches in the database");

    // Clubs
    const _clubsPromise = await context.models.Club.insertMany(CLUBS);
    console.log("Added data for", CLUBS.length, "clubs in the database");

    // Events
    const _eventsPromise = await context.models.Event.insertMany(EVENTS);
    console.log("Added data for", EVENTS.length, "events in the database");

    // Players
    const _playersPromise = await context.models.Player.insertMany(PLAYERS);
    console.log("Added data for", PLAYERS.length, "players in the database");

    console.log("Successfully seeded all data!");
  } catch (error) {
    console.log(error);
    console.log("Something went wrong when trying to seed the database.");
  }
}

module.exports = seed;
