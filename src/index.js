require("dotenv").config();
const { ApolloServer } = require("apollo-server");
const seed = require("./helpers/seed");

/* GraphQL resolvers x Schemes */
const resolvers = require("./resolvers");
const typeDefs = require("./schema/schema");

/* Mongoose Models + DB Config */
const models = require("./models");
const connectDatabase = require("./config/db");

async function _init() {
  await connectDatabase();

  const _server = new ApolloServer({
    typeDefs,
    resolvers,
    context: { models },
  });

  _server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
    console.log(`Server is ready at ${url}`);

    // Hacky seed implementation
    const arguments = process.argv.slice(2);
    if (arguments.length && arguments[0] === "--seed") {
      seed(_server.context);
    }
  });
}

_init();
