const mongoose = require("mongoose");

const _clubModel = new mongoose.Schema({
  club_name: {
    required: true,
    type: String,
  },
  image: {
    required: true,
    type: String,
  },
  league: {
    required: true,
    type: String,
  },
});

const Club = mongoose.model("Club", _clubModel);

module.exports = Club;
