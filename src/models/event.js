const mongoose = require("mongoose");

const _eventShotModel = new mongoose.Schema({
  shot_result: {
    enum: ["Goal", "Miss", "Saved", "On Target"],
    type: String,
  },
});

const _eventPassModel = new mongoose.Schema({
  end_x: {
    max: 100,
    min: 0,
    required: true,
    type: Number,
  },
  end_y: {
    max: 100,
    min: 0,
    required: true,
    type: Number,
  },
  receiving_player: {
    type: String,
    required: true,
  },
  is_cross: {
    required: true,
    type: Boolean,
  },
});

const _eventModel = new mongoose.Schema({
  id: {
    required: true,
    type: String,
  },
  club: {
    required: true,
    type: String,
  },
  formation: {
    required: true,
    type: String,
  },
  match_id: {
    required: true,
    type: Number,
  },
  player_name: {
    required: true,
    type: String,
  },
  start_x: {
    max: 100,
    min: 0,
    required: true,
    type: Number,
  },
  start_y: {
    max: 100,
    min: 0,
    required: true,
    type: Number,
  },
  timestamp: {
    // max: 90, // TODO: Figure out max value
    min: 0,
    required: true,
    type: Number,
  },
  type: {
    required: true,
    type: String,
    enum: ["shot", "pass"],
  },
  formation: {
    required: true,
    type: String, // TODO: This could/should be an enum
  },
  shot: {
    type: _eventShotModel,
  },
  pass: {
    type: _eventPassModel,
  },
});

const Event = mongoose.model("Event", _eventModel);

module.exports = Event;
