const Club = require("./club");
const Event = require("./event");
const Match = require("./match");
const Player = require("./player");

module.exports = {
  Club,
  Event,
  Match,
  Player,
};
