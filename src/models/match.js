const mongoose = require("mongoose");

const _matchModel = new mongoose.Schema({
  away_team: {
    required: true,
    type: String,
  },
  away_team_score: {
    type: Number,
  },
  date: {
    required: true,
    type: String, //TODO: fix date type
  },
  home_team: {
    required: true,
    type: String,
  },
  home_team_score: {
    type: Number,
  },
  match_id: {
    required: true,
    type: Number,
  },
});

const Match = mongoose.model("Match", _matchModel);

module.exports = Match;
