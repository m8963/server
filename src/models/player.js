const mongoose = require("mongoose");

const _playerModel = new mongoose.Schema({
  club: {
    required: true,
    type: String,
  },
  image: {
    required: true,
    type: String,
  },
  player_name: {
    required: true,
    type: String,
  },
});

const Player = mongoose.model("Player", _playerModel);

module.exports = Player;
