const queries = require("./queries");

const { eventType, date, shotResult } = require("./../schema/scalars");

module.exports = {
  EventType: eventType,
  Date: date,
  ShotResult: shotResult,
  Query: {
    clubs: queries.clubs,
    players: queries.players,
    events: queries.events,
    club: queries.club,
  },
  Club: {
    players: queries.players,
    events: queries.events,
  },
  Event: {
    match: queries.match,
  },
};
