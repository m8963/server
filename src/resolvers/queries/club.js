// Query fetching data for 1 single club
module.exports = async (parent, args, context, info) => {
  const _result = await context.models.Club.findOne({
    club_name: args.club_name,
  });

  return _result;
};
