// Query fetching all clubs
module.exports = async (parent, args, context, info) => {
  return await context.models.Club.find();
};
