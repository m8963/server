// Query fetching all events for a club of a single type
module.exports = async (parent, args, context, info) => {
  const _result = await context.models.Event.find({
    club: parent.club_name,
    type: args.eventType,
  });

  return _result;
};
