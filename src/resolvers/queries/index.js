const club = require("./club");
const clubs = require("./clubs");
const events = require("./events");
const players = require("./players");
const match = require("./match");

module.exports = {
  club,
  clubs,
  events,
  players,
  match,
};
