// Query fetching all events for a club of a single type
module.exports = async (parent, args, context, info) => {
  const _result = await context.models.Match.findOne({
    match_id: parent.match_id,
  });

  return _result;
};
