// Query fetching all players for 1 club
module.exports = async (parent, args, context, info) => {
  return await context.models.Player.find({ club: parent.club_name });
};
