const { GraphQLScalarType, Kind } = require("graphql");

const dateScalar = new GraphQLScalarType({
  name: "Date",
  description: "Custom scalar type describing a date",
  serialize(value) {
    return new Date(value);
  },
  parseValue(value) {
    //TODO: Format date correctly
    // return value.getTime();
    return new Date(value);
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return new Date(parseInt(ast.value, 10));
    }
    return null;
  },
});

module.exports = dateScalar;
