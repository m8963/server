const { GraphQLEnumType } = require("graphql");

const eventTypeEnum = new GraphQLEnumType({
  name: "EventType",
  description: "Custom scalar type describing the type of an event",
  values: {
    SHOT: {
      value: "shot",
    },
    PASS: {
      value: "pass",
    },
  },
});

module.exports = eventTypeEnum;
