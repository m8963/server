const date = require("./date");
const eventType = require("./event-type");
const shotResult = require("./shot-result");
module.exports = {
  date,
  eventType,
  shotResult,
};
