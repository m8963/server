const { GraphQLEnumType } = require("graphql");

const shotResultType = new GraphQLEnumType({
  name: "ShotResult",
  description: "Custom scalar type describing the result of a shot event",
  values: {
    GOAL: {
      value: "Goal",
    },
    MISS: {
      value: "Miss",
    },
    SAVED: {
      value: "Saved",
    },
    ON_TARGET: {
      value: "On Target",
    },
  },
});

module.exports = shotResultType;
