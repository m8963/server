const { gql } = require("apollo-server-express");

module.exports = gql`
  scalar EventType
  scalar Date
  scalar ShotResult

  type Club {
    club_name: String!
    image: String!
    league: String!
    players: [Player]
    events(eventType: EventType): [Event]
  }

  type Player {
    club: String!
    image: String!
    player_name: String!
  }

  type Match {
    away_team: String!
    away_team_score: Float
    date: Date!
    home_team: String!
    home_team_score: Float
    match_id: ID!
  }

  type EventShot {
    shot_result: ShotResult!
  }

  type EventPass {
    end_x: Float!
    end_y: Float!
    is_cross: Boolean!
    receiving_player: String!
  }

  type Event {
    _id: ID!
    club: String!
    formation: String!
    match: Match!
    match_id: ID!
    player_name: String!
    start_x: Float!
    start_y: Float!
    timestamp: Float!
    type: EventType!
    shot: EventShot
    pass: EventPass
  }

  type Query {
    clubs: [Club]
    club(club_name: String): Club
    events(club_name: String, eventType: EventType): [Event] # TODO: Add filter for type
    players(club_name: String): [Player]
    matches(club_name: String): [Match]
  }
`;
